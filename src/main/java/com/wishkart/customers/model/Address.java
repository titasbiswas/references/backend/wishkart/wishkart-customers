package com.wishkart.customers.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;


/**
 * The persistent class for the addresses database table.
 * 
 */
@Entity
@Table(name="addresses")
@NamedQuery(name="Address.findAll", query="SELECT a FROM Address a")
@Data
@Builder
@AllArgsConstructor
public class Address implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String city;

	private String country;

	private String landmark;

	@Column(name="phone_no")
	private String phoneNo;

	@Column(name="postal_code")
	private String postalCode;

	private String state;

	private String street1;

	private String street2;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="user_name", referencedColumnName="user_name")
	@JsonIgnore
	private User user;

	public Address() {
	}

}