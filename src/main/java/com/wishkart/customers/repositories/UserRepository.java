package com.wishkart.customers.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wishkart.customers.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
