package com.wishkart.customers;

import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.wishkart.customers.model.Address;
import com.wishkart.customers.model.User;
import com.wishkart.customers.repositories.AddressRepository;
import com.wishkart.customers.repositories.UserRepository;


@RunWith(SpringRunner.class)
@SpringBootTest
public class AddressRepoTest {

	@Autowired
	private AddressRepository addressRepo;
	
	@Autowired
	private UserRepository userRepo;


	@Test
	public void testSaveUser() {
		User user;
		user = User.builder().password("1234")
				.userName("titas.9i@gmail.com")
				.build();
		
		Address address;
		address = Address.builder().city("Pune")
		.country("India")
		.landmark("Near Sivaji Statue")
		.phoneNo("+91 9876543210")
		.postalCode("567899")
		.state("MH")
		.street1("50A/2 Abc Avenue")
		.street2("Kharadi").build();		
		user.addAddress(address);
		
		address = Address.builder().city("Kolkata")
				.country("India")
				.landmark("Nears friends club")
				.phoneNo("+91 9876543210")
				.postalCode("700075")
				.state("WB")
				.street1("27 Green Avenue")
				.street2("Santoshpur").build();
		user.addAddress(address);
		
		User savedUser = userRepo.save(user);
		assertNotNull(savedUser.getId());
		
		user = User.builder().password("1234")
				.userName("munmundas10@gmail.com")
				.build();
		

		address = Address.builder().city("Delhi")
		.country("India")
		.landmark("Near Summer field")
		.phoneNo("+91 9876543210")
		.postalCode("100023")
		.state("DL")
		.street1("East 34/C, 2nd Street")
		.street2("Laxmi Vihar").build();		
		user.addAddress(address);
		
		address = Address.builder().city("Mumbai")
				.country("India")
				.landmark("Behind city center")
				.phoneNo("+91 9876543210")
				.postalCode("200058")
				.state("MH")
				.street1("34 AN, Anneuxe 2, Phane 3")
				.street2("Borivali").build();
		user.addAddress(address);
		
		User savedUser2 = userRepo.save(user);

		assertNotNull(savedUser2.getId());
	}

	

}
